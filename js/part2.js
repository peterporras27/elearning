jQuery(document).ready(function() {
	var $ = jQuery;
	var run  = $('#run');
    $('[data-toggle="tooltip"]').tooltip();
    $('#start').click(function(event) {
        $(this).hide();
        start();
    });

    run.find('button').click(function(event) {
        event.preventDefault();

        if ( run.find('input').val() == 'dcpromo' ) {

            hideme('#run');
            step1();

        } else {

            speak( ['Please try again. Type in "<b>dcpromo</b>" and click "<b>OK</b>".'] );
        }

    });

    $('#dcpromo2 button').click(function(event) {
    	hideme('#dcpromo2');
    	showme('#dcpromo3');
    });

    $('#dcpromo3 button').click(function(event) {
    	step3();
    });

    $('#dcpromo4 button').click(function(event) {
    	var checked = $('#dcpromo4 input:checked').length;
    	if (checked > 0) {
    		step4();
    	} else {
    		speak(['Select the checkbox to automatically correct problems by istalling DNS service.']);
    	}    	
    });

    $('#dcpromo5 button').click(function(event) {
    	if ( $('#dcpromo5 input:checked').length > 0 ) {
    		step5();
    	} else {
    		speak(['Select "<b>Create a new domain in a new forest</b>".']);
    	}
    });

    $('#dcpromo6 button').click(function(event) {
    	if ( $('#dcpromo6 input').val() == 'sgs.local') {
    		step6();
    	} else {
    		speak(['Input "<b>sgs.local</b>" then hit <b>Next</b>.']);
    	}
    });

    $('#dcpromo8 button').click(function(e){ step8(); });
    $('#alert1 button').click(function(e){ step9(); });
    $('#dcpromo9 button').click(function(e){ step10(); });
    $('#dcpromo10 button').click(function(e){ step11(); });
    $('#dcpromo11 button').click(function(e){ step12(); });
    $('#dcpromo12 button').click(function(e){ step13(); });

    $('#alert2 input').click(function(event) {
    	if ( $('#alert2 input:checked').length == 1 ){
    		
    		$('#alert2').find('.progress-bar-fill').delay(1000).queue(function () {
		        $(this).css('width', '100%');
		    });

		    setTimeout(function(){
		    	step14();
		    },11000);

    	} else {
    		speak(['Check "<b>Reboot on completion</b>".']);
    	}
    });

    $('#dcpromo15 button').click(function(event) {
    	var css = $('#dcpromo15 input').val();
    	if ( css=='css' ) {
    		step18();
    	} else {
    		speak(['Name the Organizational Unit to "<b>CSS</b>"']);
    	}
    });

    $('#dcpromo14 .icon').click(function(event){ step19(); });

    $('#dcpromo16 .name').keyup(function(event) {
    	var txt = $(this).val();
    	$('#dcpromo16 .fullname').html(txt);
    });

    $('#dcpromo16 .logon').keyup(function(event) {
    	var txt = $(this).val();
    	$('#dcpromo16 .logonname').html(txt);
    });

    $('#dcpromo16 button').click(function(event){ step20(); });

    $('#dcpromo17 button').click(function(event) {
    	var pass = $('#dcpromo17 .password').val();
    	var confirm = $('#dcpromo17 .confirm').val();
    	var hascheck = $('#dcpromo17 input[type="checkbox"]:checked').length;
    	var error = false;
    	var messages = [];

    	if ( pass == '' || confirm == '' ) {
    		messages.push('Kindly enter your password.<br>');
    		error = true;
    	}

    	if ( pass != confirm ) {
    		messages.push('Try again.<br>Your password does not match.<br>');
    		error = true;
    	}

    	if ( hascheck==0 ) {
    		messages.push('Check at least one options in the checkboxes.');
    		error = true;
    	}

    	if ( error ) {
    		speak(messages);
    	} else {
    		step21();
    	}

    });

    $('#dcpromo18 button').click(function(event) {
    	hideme('#dcpromo14');
    	hideme('#dcpromo18');
    	speak([
    		'You\'ve Done it!',
    		'You can create another user by doing the same process again.',
    		'You may now move on the next tutorial.<br>"<a href="part3.html" style="color:#fff;text-decoration:none;"><b>Installation of the Remaining Roles and Services</b></a>"'
    	]);
    });

});

function start()
{
	speak([
		'<h3>Hi there..</h3>',
		'For this tutorial, I will guide you on how to install <b>ADDS</b> and <b>DNS</b> in your server.',
		'Lets Start!',
		'Type in "<b>dcpromo</b>" and click "<b>OK</b>".',
	], '#run', false );
}

function step1()
{
	speak(['Lets wait for the installation to finish.']);
	showme('#dcpromo1');
	setTimeout(function(){
		$('#dcpromo1').removeClass('load1');
		$('#dcpromo1').addClass('load2');
	},4000);
	$('#dcpromo1').find('.progress-bar-fill').delay(1000).queue(function () {
        $(this).css('width', '100%')
    });
	setTimeout(function(){
		step2();
	},12000);
}

function step2()
{
	hideme('#dcpromo1');
	speak(['Follow the instructions given by the installation wizard.']);
	showme('#dcpromo2');
}

function step3()
{	
	hideme('#dcpromo3');
	showme('#dcpromo4');
	speak(['Select the checkbox to automatically correct problems by istalling DNS service.']);
}

function step4()
{
	hideme('#dcpromo4');
	showme('#dcpromo5');
	speak(['Select "<b>Create a new domain in a new forest</b>".']);
}

function step5()
{
	hideme('#dcpromo5');
	showme('#dcpromo6');
	speak(['Type the Fully Qualified Domain Name (FQDN) of the new forest root domain.<br><br>Input "<b>sgs.local</b>" then hit <b>Next</b>.']);
	$('#dcpromo6 input').focus();
}

function step6()
{
	hideme('#dcpromo6');
	speak(['Checking Forest Name and Verifying NetBios Name.']);
	showme('#dcpromo7');
	setTimeout(function(){
		$('#dcpromo7').removeClass('loading1');
		$('#dcpromo7').addClass('loading2');
	},4000);
	$('#dcpromo7').find('.progress-bar-fill').delay(1000).queue(function () {
        $(this).css('width', '100%')
    });
	setTimeout(function(){
		step7();
	},12000);
}

function step7()
{
	hideme('#dcpromo7');
	showme('#dcpromo8');
	speak(['Click Next.']);
}

function step8()
{
	showme('#alert1');
	speak(['Click "<b>Yes</b>"']);
}

function step9()
{
	hideme('#alert1');
	hideme('#dcpromo8');
	showme('#dcpromo9');
	speak(['Click "<b>Next</b>"']);
}

function step10()
{
	hideme('#dcpromo9');
	showme('#dcpromo10');
	speak(['Click "<b>Next</b>"']);
}

function step11()
{
	hideme('#dcpromo10');
	showme('#dcpromo11');
	speak(['Enter your password and click "<b>Next</b>"']);
}

function step12()
{
	hideme('#dcpromo11');
	showme('#dcpromo12');
	speak(['Click "<b>Next</b>"']);
}

function step13()
{
	showme('#alert2');
	speak(['Check "<b>Reboot on completion</b>". while the waiting for the dns installation to finish.']);
}

function step14()
{
	hideme('#alert2');
	hideme('#dcpromo12');
	speakWithCallback(['Wait for the computer to restart with the latest settings configured.','Our next step is to create an <b>Organizational Unit. (OU)</b>'], 
		step15 );
}

function step15()
{
	// audio.pause(); 
	showme('#dcpromo13');
	speakWithCallback(['After the computer finishes booting up, go to <b>Start</b> > <b>Administrative Tools</b> > <b>Active Directory Users and Computers</b>'], 
		step16 );
}

function step16()
{
	// audio.pause(); 
	hideme('#dcpromo13');
	showme('#dcpromo14');
	speakWithCallback([
		'Active Directory Users and Computers window will open.',
		'Right Click on the sgs.local in the left tab.'], 
		step161 
	);
}

function step161()
{
	//audio.pause(); 
	$('#dcpromo14').removeClass('bg141');
	$('#dcpromo14').addClass('bg142');
	speakWithCallback(['Select <b>New</b> > <b>Organizational Unit</b>']);
	setTimeout(function(){
		$('#dcpromo14').removeClass('bg142');
		$('#dcpromo14').addClass('bg143');
	},3000);

	setTimeout(function(){
		step17();
	},7000);
}

function step17()
{
	
	showme('#dcpromo15');
	speak(['Lets name the Organizational Unit to "<b>CSS</b>"']);
	$('#dcpromo15 input').focus();

}

function step18()
{
	hideme('#dcpromo15');
	$('#dcpromo14').removeClass('bg143');
	$('#dcpromo14').addClass('bg144');
	speak(['We will then create two domain users: (<b>User1</b> and <b>User2</b>)<br><br>Click on the <b>user icon</b> in the upper menu bar.']);
	setTimeout(function(){
		$('#dcpromo14 .mouse').show();
		$('#dcpromo14 .mouse').addClass('slideInUp');
	},7000);
}

function step19()
{
	showme('#dcpromo16');
	speak(['Enter "<b>user1</b>" for <b>First Name</b> and <b>User Logon Name</b> and click<br><b>Next</b>.']);
}

function step20()
{
	hideme('#dcpromo16');
	showme('#dcpromo17');
	speak(['Enter User Password.']);
}

function step21()
{
	speak(['Click Finish to complete the process.']);
	hideme('#dcpromo17');
	showme('#dcpromo18');
}

