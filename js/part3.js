jQuery(document).ready(function() {
	var $ = jQuery;
	$('#start').click(function(event) {
        $(this).hide();
        start();
    });

    $('#role1 a').click(function(event){
    	step2();
    	event.preventDefault();
    });

    $('#role2 button').click(function(event){ step3(); });
    $('#role3 button').click(function(event) {

    	var cbox1 = $('#role3 .cbox1:checked').length;
    	var cbox2 = $('#role3 .cbox2:checked').length;
    	var cbox3 = $('#role3 .cbox3:checked').length;
    	var cbox4 = $('#role3 .cbox4:checked').length;

    	var message = '';
    	var err = false;

    	if(cbox1!=1){ message += 'Check DHCP Server<br>'; err = true; }
    	if(cbox2!=1){ message += 'Check Terminal Services<br>'; err = true; }
    	if(cbox3!=1){ message += 'Check Print Services<br>'; err = true; }
    	if(cbox4!=1){ message += 'Check File Services<br>'; err = true; }

    	if (err) { speak([message]); }
    	else { step4(); }

    });

    $('#role4 button').click(function(event) { step5(); });
    $('#role5 button').click(function(event) { 
    	if ( $('#role5 input:checked').length == 1 ) {
    		step6(); 
    	} else {
    		speak(['Check "<b>Terminal Server</b>" first and click "<b>Next</b>".']);
    	}
    });

    $('#role6 button').click(function(event) { step7(); });

    $('#role7 button').click(function(event) { 
    	if ( $('#role7 input:checked').length == 1 ) {
    		step8(); 
    	} else {
    		speak(['Select "<b>Do not require Network Level Authentication</b>". and click "<b>Next</b>".']);
    	}
    });

    $('#role8 button').click(function(event) { step9(); });

    $('#role9 .add').click(function(event) {
    	speak(['Click "<b>Check Names</b>" to select users.']);
    	showme('#role9v1');
    });

    $('#role9v1 .names').click(function(event) {
    	showme('#role9v2');
    	speak(['Select a user and hit OK.']);
    });

    $('#role9v2 tr').click(function(event) {

    	var txt = '';

    	if ( $(this).hasClass('blue') ) {

    		$(this).removeClass('blue');

    	} else {

    		txt = $(this).find('td').first().text();
    		$(this).addClass('blue');

    	}

    	$('#role9 .user1').html(txt);
    	$('#role9v1 .user1').html(txt);

    });

    $('#role9v1 .okie').click(function(event) {
    	hideme('#role9v1');
    	speak(['Click "Next".']);
    });

    $('#role9v2 button').click(function(event) {
    	speak(['Click "OK" again.']);
    	hideme('#role9v2');
    });

    $('#role9 .next').click(function(event){ step10(); });
    $('#role10 button').click(function(event){ step11(); });
    $('#role11 button').click(function(event){ step12(); });

    $('#role12 .validate').click(function(event) {
    	showme('#role12 img');
        speak(['After validation Click "Next".']);
    });

    $('#role12 .next').click(function(event) {
        step13();
    });

});

function start()
{
	speakWithCallback([
		'Hey there, on our next turorial I will guide you through the Installation of the Remaining Roles and Services.',
		'The first thing we should do is to open up "<b>Server Manager</b>".'
	], step1);
}

function step1()
{
	showme('#role1');
	speak(['Select "<b>Roles</b>" in the upper left corner of window.<br><br>Click "<b>Add Roles</b>".']);
	setTimeout(function(){
		$('#role1').removeClass('role1v1');
		$('#role1').addClass('role1v2');
		$('#role1 a').show();
	},5000);
}

function step2()
{
	hideme('#role1');
	showme('#role2');
	speak(['Before you begin click "<b>Next</b>".']);
}


function step3()
{
	hideme('#role2');
	showme('#role3');
	speak([
		'Select the items:<br>'+
		'1.) <b>DHCP Server</b></br>'+
		'2.) <b>Terminal Services</b></br>'+
		'3.) <b>Print Services</b></br>'+
		'4.) <b>File Services</b></br>',
	]);
}

function step4()
{
	hideme('#role3');
	showme('#role4');
	speak(['Click "<b>Next</b>".']);
}

function step5()
{
	hideme('#role4');
	showme('#role5');
	speak(['Check "<b>Terminal Server</b>" and click "<b>Next</b>".']);
}

function step6()
{
	hideme('#role5');
	showme('#role6');
	speak(['Click "<b>Next</b>".']);
}

function step7()
{
	hideme('#role6');
	showme('#role7');
	speak(['Select "<b>Do not require Network Level Authentication</b>"<br> and click "<b>Next</b>".']);
}

function step8()
{
	hideme('#role7');
	showme('#role8');
	speak(['Click "<b>Next</b>".']);
}

function step9()
{
	hideme('#role8');
	showme('#role9');
	speak(['Click "<b>Add</b>" to select user groups allowed to access the terminal server.']);
}

function step10()
{
	hideme('#role9');
	showme('#role10');
	speak(['Click "<b>Next</b>".']);
}

function step11()
{
	hideme('#role10');
	showme('#role11');
	speak(['Click "<b>Next</b>".']);
}

function step12()
{
	hideme('#role11');
	showme('#role12');
	speak(['Click "<b>Validate</b>" to check if the preffered DNS server is active.']);
}

function step13()
{
    hideme('#role12');
    showme('#role13');
    speak(['Click "<b>Validate</b>" to check if the preffered DNS server is active.']);
}


