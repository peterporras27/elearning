var chatbox = jQuery('#typed-strings');
var strings = [];
var audios = ['speak0.mp3','speak1.mp3','speak2.mp3','speak3.mp3'];
var sounds = audios[Math.floor(Math.random()*audios.length)];
var audio = new Audio('sounds/'+sounds);
audio.onended = function (){
    var sounds = audios[Math.floor(Math.random()*audios.length)];
    audio.src = 'sounds/'+sounds;
    audio.load();
    audio.play();
};

function showme( target )
{
    var target = $(target);
    target.show();
    target.removeClass('fadeOut');
    target.addClass('fadeIn');
}

function hideme( target )
{   
    var target = $(target);
    target.removeClass('fadeIn');
    target.addClass('fadeOut');
    target.hide();
}
/*
function hideall()
{
    $('#start').hide();
    $('#computer .animated').each(function(index, el) {
        var id = el.attr('id');       
        hideme('#'+id);
    });
}*/

function speak( strings, showTarget = false, hideTarget = false )
{
    //audio.play();

    jQuery("#typed").typed({
        strings: strings,
        typeSpeed: 30,
        backDelay: 500,
        backSpeed: -100,
        loop: false,
        contentType: 'html',
        loopCount: false,
        callback: function(){

            //audio.pause();  

            if ( hideTarget != false ) {
                hideme(hideTarget);
            }

            if ( showTarget != false ) {
               showme(showTarget);
            }

        }
    });    
}

function speakWithCallback( strings, customCallback )
{
    //audio.play();

    jQuery("#typed").typed({
        strings: strings,
        typeSpeed: 30,
        backDelay: 500,
        backSpeed: -100,
        loop: false,
        contentType: 'html',
        loopCount: false,
        callback: customCallback
    });    
}

function isValidIp(ipaddress) {  
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {  
        return (true)  
    }  
    return (false)  
}  