function startTalk()
{
    var run  = $('#run');
    
    var strings = ['<h1>Hi There!</h1>'];
    strings.push("Today I'm going to guide you through on how to setup <b>Computer Servers</b> in your machine.");
    strings.push("On the first part, we'll do the <b>Initial Configuration Task on the Server</b>.");
    strings.push("Let's get Started!");
    strings.push('First let\'s run "<b>ncpa.cpl</b>".');
    
    speak(strings, run );
}

function nextText1()
{
    audio.pause();  
    $('#netcon button').click(function(event) {
        showme('#netcon1');
        strings = ['Select Internet Protocol Version 4 (TCP/IPv4) then click <b>Properties</b>.'];
        speakWithCallback( strings, nextText2 );
    });
}

function nextText2()
{
    audio.pause();
    $('#netcon1 button').click(function(event) {
        audio.pause();  
        showme('#netcon2');
        strings = ['Enter IP address <b>192.168.028.003</b>,<br>Subnet Mask <b>255.255.255.000</b>,<br> and the Default Gateway <b>192.168.028.001</b>'];
        speak( strings, '#netcon2', false );
    });
}

function nextText3()
{
    audio.pause();
    $('#computer').removeClass('boot');
    $('#computer').addClass('login');
    strings = ['After signing in as administartor, We will then begin configuring your firewall settings.'];
    speakWithCallback( strings, nextText4 );
}

function nextText4()
{
    $('#computer').removeClass('login');
    strings = ["Lets turn off the Windows firewall first<br> and open run window."];
    speakWithCallback( strings, nextText5 );
}

function nextText5()
{
    showme('#run2');
    strings = ['Type "<b>firewall.cpl</b>". and click "<b>OK</b>".'];
    speak( strings, false, false );
}

function nextText6()
{
    showme('#run3');
    strings = ['The next step is to ping test our Network connectivity.','Type "<b>cmd</b>". and click "<b>OK</b>" to open command line interface.'];
    speak( strings, false, false );
}

function finish()
{
    speak( [
        'Well done!',
        'Follow the same procedures when checking Wireless Access Point, Desktop Computer and Laptop Computer.',
        'You may now preceed to the next topic: "<b><a href="part2.html" style="color:#fff;text-decoration:none;">ADDS and DNS Installation</a></b>"'], 
        false, false 
    );
}

jQuery(document).ready(function() {
    var $ = jQuery;
    var run  = $('#run');
    $('[data-toggle="tooltip"]').tooltip();
    $('#start').click(function(event) {
        $(this).hide();
        startTalk();
    });

    run.find('input').focus(function(event) {
        strings = ['Type in "<b>ncpa.cpl</b>" and click "<b>OK</b>".'];
        speak( strings );
    });

    run.find('button').click(function(event) {
        event.preventDefault();

        if ( run.find('input').val() == 'ncpa.cpl' ) {

            hideme('#run');

            showme('#netcon');

            strings = [
                'A Network connection window will popup showing your local area connection.<br>',
                'Right click on the icon and click <b>Properties</b>.'
            ];

            speakWithCallback( strings, nextText1 );

        } else {

            strings = ['Please try again. Type in "<b>ncpa.cpl</b>" and click "<b>OK</b>".'];
            speak( strings, run, false );
        }

    });

    $('#netcon2 input').blur(function(event) {
        var ip = $(this).val().replace(/[^\d.]+/g, '');
        if ( isValidIp(ip) ) { $(this).val(ip); } 
        else { $(this).val('000.000.000.000'); }
    });

    $('#netcon2 button').click(function(event) {
        var ip = $('#netcon2 .ip').val();
        var mask = $('#netcon2 .mask').val();
        var gateway = $('#netcon2 .gateway').val();

        if ( isValidIp(ip) && isValidIp(ip) && isValidIp(ip) ) {

            hideme('#netcon2');

            setTimeout(function(){
                hideme('#netcon1');
            },1000);

            setTimeout(function(){
                hideme('#netcon');
            },2000);
            
            strings = [
                "You've done it!",
                "On our next step let's change the computer name of the server to \"<b>server28</b>\"",
                "Using your keyboard; press <br>\"<b>Winkey + Pause Break</b>\" (keyboard shortcut) to open <b>Control Panel System</b>.<br><br>Click <b>Change Settings</b>"];
            speak( strings, '#server28', false );
        }
    });

    $('#server28 .changesettings').click(function(event) {
        event.preventDefault();
        showme('#server281');
        strings = ["To rename this computer or change its domain or workgroup. <b>Click Change</b>."];
        speak( strings, false, false );
    });

    $('#server281 button').click(function(event) {
        showme('#server282');
        strings = ["Change the Computer name to \"<b>Server28</b>\" and click <b>OK</b>" ];
        speak( strings, false, false );
    });

    $('#server282 button').click(function(event) {
        var serverName = $('#server282 input').val();
        if ( serverName != '' ) {
            showme('#server283');
            strings = ["An alert box will notify you to restart. Click <b>OK</b>."];
            speak( strings, false, false );
        }
    });

    $('#server283 button').click(function(event) {
        showme('#server284');
        strings = ["Click <b>Restart Now</b>."];
        speak( strings, false, false );
    });

    $('#server284 button').click(function(event) {
        $('#computer .animated').removeClass('fadeIn');
        $('#computer .animated').addClass('fadeOut');
        strings = ["Restarting now...",'. . . . . . . . . . . . . . . . . . .'];
        speakWithCallback( strings, nextText3 );
        $('#computer').addClass('boot');
    });
    
    $('#run2 button').click(function(event) {
        var runtxt = $('#run2 input').val();
        if ( runtxt!='' ) {
            hideme('#run2');
            showme('#firewall');
            strings = ['Click on the "<b>Change Settings</b>" button to update firewall.'];
            speak( strings, false, false );
        }
    });

    $('#firewall button').click(function(event) {

        showme('#firewall1');
        strings = ['Turn the firewall off by selecting "off".'];
        speak( strings, false, false );
    });

    $('#firewall1 button').click(function(event) {

        var stat = $('#firewall1 input:checked').val();
        console.log( stat );
        
        if ( stat == 'off' ) {
            strings = ['You\'ve Done it!'];
        } else {
            strings = ['Please select "off" to turn off firewall.'];
        }

        speak( strings, false, false );

        if (stat=='off') { 
            hideme('#firewall1'); 
            hideme('#firewall'); 
            nextText6();
        }

    });

    $('#run3 button').click(function(event) {
        hideme('#run3');
        showme('#ping');
        $('#ping input').focus();
        strings = [
            'Here\'s a list of items we need to do.<br>1.) Ping Wireless Router.<br>2.) Ping Wireless Access Point.<br>3.) Ping Desktop Computer.<br>4.) Ping Laptop Computer.',
            '1.) Ping Wireless Router.<br><br>Use your router IP (ie. 192.168.28.1)<br> type "ping 192.168.28.1" and hit <b>Enter</b>'];
        speak( strings, false, false );
    });

    $('#ping input').keypress(function(e) {
        if(e.which == 13) {

            if( $(this).val()!='ping 192.168.28.1' ){
                speak( ['Type "ping 192.168.28.1" and hit <b>Enter</b>'], false, false );
            } else {
                var stat = "Ping statistics for 192.168.28.1:<br>"+
                    "&nbsp;&nbsp;Packets: Sent = 4, Recieved = 4, Lost = 0 (0% loss),<br>"+
                    "Approximate round trip times in milli-seconds:<br>"+
                    "&nbsp;&nbsp;Minimum = 0ms, Maximum = 7ms, Average = 2ms";
                
                $('#ping div').html('<br>Pinging 192.168.28.1 with 32 bytes of data:<br>');
                setTimeout(function(){$('#ping div').html( $('#ping div').html()+'Reply from 192.168.28.1: bytes=32 time=1ms TTL=64<br>');},2000);
                setTimeout(function(){$('#ping div').html( $('#ping div').html()+'Reply from 192.168.28.1: bytes=32 time=7ms TTL=64<br>');},4000);
                setTimeout(function(){$('#ping div').html( $('#ping div').html()+'Reply from 192.168.28.1: bytes=32 time<1ms TTL=64<br>');},6000);
                setTimeout(function(){$('#ping div').html( $('#ping div').html()+'Reply from 192.168.28.1: bytes=32 time=1ms TTL=64<br><br>');},8000);
                setTimeout(function(){$('#ping div').html( $('#ping div').html()+stat ); finish(); },10000);
                $(this).blur();
            }
        }
    });

    $('#ping input').blur(function(e) {
        if( $(this).val()!='ping 192.168.28.1' ){
            speak( ['Type "ping 192.168.28.1" and hit <b>Enter</b>'], false, false );
            $('#ping input').focus();
        }
    });

});
